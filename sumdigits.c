#include <stdio.h>
int input()
{   
    int a;
    printf("Enter the number\n");
    scanf("%d",&a);
    return a;
}
 int compute(int num)
{
      int rem,sum=0;
      for(;num>0;num=num/10)
      {
          rem=num%10;
          sum=sum+rem;
      }
       return sum;
}
void output(int a)
{
    
     printf("The sum of the digits of the number is %d/n",a);
}
int main()
{ 
    output(compute(input()));
    return 0;
}