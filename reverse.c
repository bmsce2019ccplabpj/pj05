#include <stdio.h>
int input()
{   
    int a;
    printf("Enter the number\n");
    scanf("%d",&a);
    return a;
}
 int compute(int num)
{
      int rem,rev=0;
      while(num>0)
      {
          rem=num%10;
          rev=rev*10+rem;
          num=num/10;
      }
       return rev;
}
void output(int a,int b)
{
    
     printf("The reverse of the number is %d/n",a);
     if (a==b)
         printf ("The number is palindrome");
         else printf("The number is not palindrome ");
    
}
int main()
{ 
    int x,z;
    x=input();
    z=compute(x);
    output(z,x);
    return 0;
}