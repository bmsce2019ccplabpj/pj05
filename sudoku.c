#include <stdio.h>
int inputn()
{
    int n;
    printf("enter how many sudokus\n");
    scanf("%d",&n);
    return n;
}
void input(int n,int a[n][9][9],int i)
{
    for(int j=0;j<9;j++)
    {
        for(int k=0;k<9;k++)
            scanf("%d",&a[i][j][k]);
    }
}
void checkrow(int n,int a[n][9][9],int i, int r[i][9],int rbuild[i])
{
    for(int row=0;row<9;row++)
        for(int j=0;j<9;j++)
            for(int k=j+1; k<9; k++)
                if(a[i][row][j]==a[i][row][k]&&a[i][row][j]!=0)
                {
                    r[i][row]=row+1;
                    rbuild[i]=1;
                    break;
                }
}
void checkcolumn(int n,int a[n][9][9],int i, int c[n][9],int cbuild[n])
{
    for(int column=0;column<9;column++)
     for(int j=0;j<9;j++)
        for(int k=j+1; k<9; k++)
            if(a[i][j][column]==a[i][k][column]&&a[i][j][column]!=0)
                {
                    c[i][column]=column+1;
                    cbuild[i]=1;
                    break;
                }
}
void checksubmatrix(int n,int a[n][9][9],int i, int sb[n][9],int sbuild[n], int checkzero[n])
{
    int indexsub=0;
    for(int aa=0;aa<7;aa+=3)
        for(int b=0;b<7;b+=3)
        {
            int rep=0;
            for(int i1=aa;i1<aa+3;i1++)
                for(int i2=b;i2<b+3;i2++)
                {
                    for(int i3=aa;i3<aa+3;i3++)
                        for(int i4=b;i4<b+3;i4++)
                            if( a[i][i1][i2] == a[i][i3][i4] && a[i][i1][i2]!=0)
                                rep++;

                    if(a[i][i1][i2]==0)
                    {
                        rep++;
                        checkzero[i]=1;
                    }
                }
                if (rep>9)
                    sb[i][indexsub]=indexsub+1;
             indexsub++;
        }
         for(int b=0;b<9;b++)
         {
            sbuild[i]=0;
            if (sb[i][b]!=0)
            {
                sbuild[i]=1;
                break;
            }
        }
}
void printsudoku(int n,int i,int rbuild[n],int cbuild[n],int sbuild[n], int checkzero[n],int r[n][9],int c[n][9],int sb[n][9])
{
    int build=1;
    if(rbuild[i]||cbuild[i]||sbuild[i]==1)
        build =0;
    if (build==1 && checkzero[i]==0)
            printf("\n\ncomplete");
        else if(build==1 && checkzero[i]==1)
            printf("\n\nincomplete but viable");
        else
        {
            printf("\n\nNot viable\nrows:");
        for(int i1=0;i1<9;i1++)
            if (r[i][i1]!=0)
                printf("%d ",r[i][i1]);

            printf("\ncolumns:");
        for(int i1=0;i1<9;i1++)
            if (c[i][i1]!=0)
                printf("%d ",c[i][i1]);

            printf("\nSub matrix:");
        for(int i1=0;i1<9;i1++)
            if (sb[i][i1]!=0)
                printf("%d ",sb[i][i1]);
        }
}
int main()
{
    int n=inputn();
    int a[n][9][9];
    int r[n][9],c[n][9],sb[n][9],checkzero[n],rbuild[n],cbuild[n],sbuild[n];
    for(int i=0;i<n;i++)
    for(int j=0;j<9;j++)
        {
            r[i][j]=0;
            c[i][j]=0;
            sb[i][j]=0;
            rbuild[j]=0;
            cbuild[j]=0;
            sbuild[j]=0;
            checkzero[j]=0;
        }
    for(int i=0;i<n;i++)
    {
    input(n,a,i);
    printf("\n \n");
    checkrow(n,a,i,r,rbuild);
    checkcolumn(n,a,i,c,cbuild);
    checksubmatrix(n,a,i,sb,sbuild,checkzero);
    }
    for(int i=0;i<n;i++)
    printsudoku(n,i,rbuild,cbuild,sbuild,checkzero,r,c,sb);
}