import os
import tempfile
import sys
import urllib2
import json
import base64
import logging
import re
import pprint
import requests
import subprocess

projectKey= "FW"
repoKey = "fw"
branch = "master"
pathToVersionProperties = "core/CruiseControl/CI_version.properties"
localVersionProperties = "CI_version.properties"
bitbucketBaseUrl = "https://bitbucket.company.com/rest/api/latest"
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

def checkPersonalAccessToken():
      try:
         os.environ["PAT"]
         logging.info("Detected Personal Access Token")
      except KeyError: 
         logging.error("Personal Access Token: $PAT env variable not set, update Jenkins master with correct environment variable")
         sys.exit(1)

def getJenkinsPropertiesFile():
    restEndpoint = "{}/projects/{}/repos/{}/raw/{}".format(bitbucketBaseUrl, projectKey, repoKey, pathToVersionProperties)
    logging.info("REST endpoint : {}".format(restEndpoint))
    request = urllib2.Request(restEndpoint)
    request.add_header("Authorization", "Bearer %s" % os.environ["PAT"])
    result = urllib2.urlopen(request).read()
    return result

checkPersonalAccessToken()
propertiesString = getJenkinsPropertiesFile()