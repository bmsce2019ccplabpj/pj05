
#include <stdio.h>

struct frac
{
    int n,d;
};
void input(int *size)
{
    printf("Enter the number of fractions that needs to be added\n");
    scanf("%d",size);
}
void fractioninput(struct frac a[], int size)
{
    for(int i=0;i<size;i++)
    {
        printf("Enter the fraction %d\n",i+1);
        scanf("%d%d",&a[i].n,&a[i].d);
    }
}

int gcd(int a, int b)
{
    int gcd=1;
    for(int i=1;i<=a&&i<=b;i++)
    {
        if(a%i==0 && b%i==0)
            gcd=i;
    }
    return gcd;
}

void compute(struct frac a[],int size,int *n3,int *d3)
{
    int g,lcm,cf,tempn,tempd;
    *n3=a[0].n;
    *d3=a[0].d;
    for(int i=1;i<size;i++)
    {
        g=gcd(*d3,a[i].d);
        lcm=(*d3*a[i].d)/g;
        *n3=(((*n3)*lcm)/(*d3)) + ((a[i].n*lcm)/a[i].d);
        *d3=lcm;
        cf=gcd(*n3,*d3);
        *n3=*n3/cf;
        tempn=*n3;
        *d3=*d3/cf;
        tempd=*d3;
    }    
}   

void print(int n3, int d3)
{
    printf("THe sum of the fraction is %d/%d",n3,d3);
}
int main()
{
    int size;
    input(&size);
    struct frac a[size],res;
    fractioninput(a,size);
    compute(a,size,&res.n,&res.d);
    print(res.n,res.d);
    return 0;
}