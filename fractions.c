
#include <stdio.h>

void input(int *n1, int *n2, int *d1, int *d2)
{
    printf("Enter the fraction 1\n");
    scanf("%d%d",n1,d1);
    printf("Enter the fraction 2\n");
    scanf("%d%d",n2,d2);
}

int gcd(int a, int b)
{
    int gcd=1;
    for(int i=1;i<=a&&i<=b;i++)
    {
        if(a%i==0 && b%i==0)
        gcd=i;
    }
    return gcd;
}

int compute(int n1, int n2, int d1, int d2, int *n3, int *d3)
{
    int g,lcm,cf;
    g=gcd(d1,d2);
    lcm=(d1*d2)/g;
    *n3=((n1*lcm)/d1) + ((n2*lcm)/d2);
    *d3=lcm;
    cf=gcd(*n3,*d3);
    *n3=*n3/cf;
    *d3=*d3/cf;
}

void print(int n3, int d3)
{
    printf("THe sum of the fraction is %d/%d",n3,d3);
}
int main()
{
    struct frac
    {
        int n,d;
    } one,two,res;
    input(&one.n,&two.n,&one.d,&two.d);
    compute(one.n,two.n,one.d,two.d,&res.n,&res.d);
    print(res.n,res.d);
    return 0;
}